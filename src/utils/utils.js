//格式化时间格式成为字符串

export const formatDate = (strFormat, date) => {
    try {
        if (date === undefined) {
            var curDate = new Date();
        } else if (!(date instanceof Date)) {
            console.log('你输入的date:' + date + '不是日期类型');
            return date;
        } else {
            curDate = date;
        }
        return formatCurrentDate(strFormat, curDate);
    } catch (e) {
        console.log('格式化日期出现异常：' + e.message);
    }
};

export const getDateObject = (date) => {
    return {
        'M+': date.getMonth() + 1,
        'd+': date.getDate(),
        'H+': date.getHours(),
        'm+': date.getMinutes(),
        's+': date.getSeconds()
    };
}


export const formatCurrentDate = (strFormat, date) => {
    var tempFormat = strFormat === undefined ? "yyyy-MM-dd HH:mm:ss" : strFormat;
    var dates = getDateObject(date);
    if (/(y+)/.test(tempFormat)) {
        var fullYear = date.getFullYear() + '';
        var year = RegExp.$1.length === 4 ? fullYear : fullYear.substr(4 - RegExp.$1.length);
        tempFormat = tempFormat.replace(RegExp.$1, year);
    }
    for (var i in dates) {
        if (new RegExp('(' + i + ')').test(tempFormat)) {
            var target = RegExp.$1.length === 1 ? dates[i] : ('0' + dates[i]).substr(('' + dates[i]).length - 1);
            tempFormat = tempFormat.replace(RegExp.$1, target);
        }
    }
    return tempFormat === strFormat ? date.toLocaleString() : tempFormat;
};


//格式化时间格式 字符串成为时间对象
export const parseDate = (strFormat, date) => {
    if (typeof date !== 'string') {
        return new Date();
    }
    var longTime = Date.parse(date);
    if (isNaN(longTime)) {
        if (strFormat === undefined) {
            console.log('请输入日期的格式');
            return new Date();
        }
        var tmpDate = new Date();
        var regFormat = /(\w{4})|(\w{2})|(\w{1})/g;
        var regDate = /(\d{4})|(\d{2})|(\d{1})/g;
        var formats = strFormat.match(regFormat);
        var dates = date.match(regDate);
        if (formats !== undefined && dates !== undefined && formats.length === dates.length) {
            for (var i = 0; i < formats.length; i++) {
                var format = formats[i];
                if (format === 'yyyy') {
                    tmpDate.setFullYear(parseInt(dates[i], 10));
                } else if (format === 'yy') {
                    var prefix = (tmpDate.getFullYear() + '').substring(0, 2);
                    var year = (parseInt(dates[i], 10) + '').length === 4 ? parseInt(dates[i], 10) : prefix + (parseInt(dates[i], 10) + '00').substring(0, 2);
                    var tmpYear = parseInt(year, 10);
                    tmpDate.setFullYear(tmpYear);
                } else if (format === 'MM' || format === 'M') {
                    tmpDate.setMonth(parseInt(dates[i], 10) - 1);
                } else if (format === 'dd' || format === 'd') {
                    tmpDate.setDate(parseInt(dates[i], 10));
                } else if (format === 'HH' || format === 'H') {
                    tmpDate.setHours(parseInt(dates[i], 10));
                } else if (format === 'mm' || format === 'm') {
                    tmpDate.setMinutes(parseInt(dates[i], 10));
                } else if (format === 'ss' || format === 's') {
                    tmpDate.setSeconds(parseInt(dates[i], 10));
                }
            }
            return tmpDate;
        }
        return tmpDate;
    } else {
        return new Date(longTime);
    }
};
