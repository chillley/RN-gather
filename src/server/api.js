import axios from './axios';
import qs from 'qs';

//登陸
export const login = (params) => axios({
    url: 'api/user/login',
    method: 'post',
    data: qs.stringify(params),
});
//日程
export const getScheduleToday = (params = {}) => axios({
    url: 'api/schedule/getScheduleToday',
    method: 'post',
    data: qs.stringify(params),
});
//笔记
export const getNote = (params = {}) => axios({
    url: 'api/note/getNote',
    method: 'post',
    data: qs.stringify(params),
});
