import * as axios from 'axios';
import {Tip} from 'beeshell';


const sBaseUrl = 'http://192.168.113.22:7001/';

let instance = axios.create({
    baseURL: sBaseUrl,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    withCredentials: true, //允许携带token
});

instance.interceptors.request.use(async function (config) {
    return config;
}, function (error) {
    return Promise.reject(error);
});


instance.interceptors.response.use(function (response) {
    const res = response.data;
    if (res.code != '200') { //说明后台抛异常了
        let sMessage = '系统错误';
        if (res.message) {
            sMessage = res.message;
        }
        Tip.show(sMessage);
        return Promise.reject(sMessage);
    }
    return res;
}, function (error) {
    /*本地开发环境因为跨域拿不到axios返回的错误信息*/
    return Promise.reject(error);
});

export default instance;
