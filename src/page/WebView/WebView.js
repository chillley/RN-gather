import React, {Component} from 'react';
import {StyleSheet, View, StatusBar, Platform, BackHandler} from 'react-native';
import {WebView} from 'react-native-webview';
import {connect} from 'react-redux';
import MixinStyle from '../../style/MixinStyle';
import {Progress} from 'beeshell';
import Header from "../../components/Header"

class RnWebView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
            title: "",
            backButtonEnabled: false
        };
        this.webview = null;
        this.addBackAndroidListener();
    }

    componentDidMount() {
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('light-content');
            StatusBar.setTranslucent(true);
        });
        this.props.navigation.setParams({//给导航中增加监听事件
            goBackPage: this.goBackPage
        });
    }

    goBackPage = () => {
        if (this.state.backButtonEnabled) {
            this.webview.goBack();
        } else {//否则返回到上一个页面
            this.nav.goBack();
        }
    }

    componentWillUnmount() {
        this._navListener.remove();
        BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid);
    }

    addBackAndroidListener = () => {
        if (Platform.OS === 'android') {
            BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
        }
    }

    handlePressBack = () => {
        const {navigation} = this.props;
        if (navigation) {
            navigation.goBack();
        }
    }
    onNavigationStateChange = (navState) => {
        this.setState({
            backButtonEnabled: navState.canGoBack
        });
    }

    onBackAndroid = () => {
        if (this.state.backButtonEnabled) {
            this.webview.goBack();
            return true;
        } else {
            return false;
        }
    };

    render() {
        const webViewUri = this.props.navigation.getParam('uri', "http://47.111.14.175:8081/Index")
        return (
            <View style={{...styles.wrapper}}>
                <StatusBar translucent={true} StatusBarAnimation={'none'}/>
                <Header leftIcon={'left'} title={this.state.title} leftIconPress={this.handlePressBack} rightIcon={'more'}/>
                <View style={styles.titleBar}>
                    {this.state.progress !== 1 && <Progress easing={true}
                                                            style={styles.progress}
                                                            percent={this.state.progress}
                                                            barStyle={{height: 2}}/>}
                </View>


                <WebView
                    ref={ref => this.webview = ref}
                    style={styles.wrapper} source={{uri: webViewUri}}
                    onNavigationStateChange={this.onNavigationStateChange}
                    onLoadStart={(syntheticEvent) => {
                        const {nativeEvent} = syntheticEvent;
                        this.setState(
                            {title: nativeEvent.title || webViewUri},
                        );
                    }}
                    injectedJavaScript={`
                             (function() {
                              function wrap(fn) {
                                       return function wrapper() {
                                       var res = fn.apply(this, arguments);
                                       window.ReactNativeWebView.postMessage('navigationStateChange');
                                       return res;
                                       }
                              }
                               history.pushState = wrap(history.pushState);
                               history.replaceState = wrap(history.replaceState);
                               window.addEventListener('popstate', function() {
                               if(history.length === 1){
                                     window.ReactNativeWebView.postMessage('navigationGoBack');
                               }else{
                                window.ReactNativeWebView.postMessage('navigationStateChange');
                               }
                              });
                             })();
                            true;
                   `}
                    onMessage={({nativeEvent: state}) => {
                        if (state.data === 'navigationStateChange') {
                            // Navigation state updated, can check state.canGoBack, etc.
                            this.onNavigationStateChange({canGoBack: true})
                        } else if (state.data === 'navigationGoBack') {
                            this.nav.goBack();
                        }
                    }}
                    onLoadProgress={({nativeEvent}) => {
                        this.setState(
                            {progress: nativeEvent.progress},
                        );
                    }}/>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
    titleBar: {
        height: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },

    progress: {
        position: 'absolute',
        padding: 0,
        margin: 0,
        bottom: 0,
        left: 0,
        width: MixinStyle.size.screenWidth,
        backgroundColor: '#4fff20'
    },
});


const mapStateToProps = state => ({
    app: state.app,
});

export default connect(mapStateToProps)(RnWebView);
