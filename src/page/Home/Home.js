import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    ScrollView,
    RefreshControl,
    StatusBar,
    TouchableWithoutFeedback, TouchableOpacity, PixelRatio,

} from 'react-native';
import {connect} from 'react-redux';
import AntDesignIcon from "react-native-vector-icons/AntDesign"; //使用antDesign图标
import LinearGradient from "react-native-linear-gradient"
import MixinStyle from '../../style/MixinStyle'
import Swiper from 'react-native-swiper';
import {Badge, WingBlank, WhiteSpace, Button} from '@ant-design/react-native';
import StepIndicator from 'react-native-step-indicator';
import {ViewPager} from 'rn-viewpager'
import _ from "lodash"

import {getScheduleToday, getNote} from "../../server/api";
import {formatDate, parseDate} from "../../utils/utils";

const mapStateToProps = state => ({});


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            banner: [
                {
                    src: require('../../assets/home/banner1.jpg')
                }, {
                    src: require('../../assets/home/banner2.jpg')
                },
            ],
            aColorArray: [["#255390", "#213075"], ["#c26d4b", '#C23555']],
            colors: ["#255390", "#213075"],
            scheduleTimeLabels: [],
            schedule: [],
            note: [],
            currentPage: 0
        };
    }

    componentDidMount() {
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('light-content');
            StatusBar.setTranslucent(true);
            StatusBar.setBackgroundColor('transparent');
        });
        getScheduleToday().then(res => {
            if (res.success) {
                let aSchedule = _.orderBy(res.data, ['start_at', 'age']);
                const timeLabels = _.map(aSchedule, (item) => {
                    return `${item.start_at.substring(item.start_at.length - 8)} ~ ${item.end_at.substring(item.end_at.length - 8)}`
                });
                let currentPage = 0;
                aSchedule.forEach((row, index) => {
                    const nowDate = new Date();
                    const startDate = parseDate("yyyy-MM-dd HH:mm:ss", row.start_at);
                    if (nowDate.getTime() > startDate.getTime()) {
                        currentPage = index
                    }
                });

                this.setState({
                    scheduleTimeLabels: timeLabels,
                    schedule: aSchedule,
                    currentPage: currentPage
                })
                this.viewPager.setPage(currentPage)
            }
        });
        getNote().then(res => {
            if (res.success) {
                this.setState({
                    note: res.data
                })
            }
        })
    }


    componentWillReceiveProps(nextProps, nextState) {
        if (nextState.currentPage != this.state.currentPage) {
            if (this.viewPager) {
                this.viewPager.setPage(nextState.currentPage)
            }
        }
    }

    componentWillUnmount() {
        this._navListener.remove();
    }

    onStepPress = position => {
        this.setState({currentPage: position})
        this.viewPager.setPage(position)
    }
    renderViewPagerSchedule = schedule => {
        return (
            <View style={{flex: 1, marginTop: 5}}>
                <Text style={{
                    alignItems: 'center',
                    color: '#000',
                    fontSize: MixinStyle.fontSize.regular
                }}>{schedule.title}</Text>
                <Text style={{
                    alignItems: 'center',
                    color: '#000',
                    fontSize: MixinStyle.fontSize.tiny
                }}>{schedule.content}</Text>
            </View>)
    }
    bannerIndexChange = index => {
        this.setState({
            colors: this.state.aColorArray[index]
        })
    }
    onScroll = (event) => {
        let y = event.nativeEvent.contentOffset.y
        let opacityPercent = y / 200
        if (y < 200) {
            this.navBar.setNativeProps({
                style: {opacity: opacityPercent}
            });
            // if (opacityPercent === 0) {
            //     StatusBar.setBackgroundColor(`transparent`);
            // } else {
            //     StatusBar.setBackgroundColor(`rgba(51,136,255,${opacityPercent})`);
            // }
        } else {
            this.navBar.setNativeProps({
                style: {opacity: 1}
            })
            // StatusBar.setBackgroundColor(`rgba(51,136,255,${1})`);
        }
    }
    goSearch = () => {
        this.props.navigation.push('Search');
    }
    goScannerView = () => {
        this.props.navigation.push('WebView');
    }

    render() {
        const {banner, colors, scheduleTimeLabels, schedule, note} = this.state;
        const labels = scheduleTimeLabels;
        const stepCount = labels.length;
        const customStyles = {
            stepIndicatorSize: 15,
            currentStepIndicatorSize: 16,
            separatorStrokeWidth: 2,
            currentStepStrokeWidth: 3,
            stepStrokeCurrentColor: '#38f',
            stepStrokeWidth: 3,
            stepStrokeFinishedColor: '#38f',
            stepStrokeUnFinishedColor: '#dedede',
            separatorFinishedColor: '#38f',
            separatorUnFinishedColor: '#dedede',
            stepIndicatorFinishedColor: '#38f',
            stepIndicatorUnFinishedColor: '#ffffff',
            stepIndicatorCurrentColor: '#ffffff',
            stepIndicatorLabelFontSize: 0,
            currentStepIndicatorLabelFontSize: 0,
            stepIndicatorLabelCurrentColor: 'transparent',
            stepIndicatorLabelFinishedColor: 'transparent',
            stepIndicatorLabelUnFinishedColor: 'transparent',
            labelColor: '#000',
            labelSize: 13,
            labelFontFamily: 'OpenSans-Italic',
            currentStepLabelColor: '#38f'
        }
        return (
            <View style={styles.container}>
                <StatusBar translucent={true} networkActivityIndicatorVisible={true}
                           StatusBarAnimation={'none'}/>
                <View style={[styles.searchContainer, {opacity: 1, backgroundColor: 'transparent'}]}
                      ref={ref => this.navBar = ref}>
                    <View style={styles.searchInput}>
                        <TouchableWithoutFeedback onPress={this.goSearch}>
                            <View style={styles.inputWrap}>
                                <AntDesignIcon name={'search1'} style={{fontSize: 15, marginRight: 5}}/>
                                < Text style={styles.input}>
                                    请输入标题或内容搜索...</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    <View>
                        <View style={styles.iconWrap}>
                            <TouchableWithoutFeedback onPress={this.goScannerView}>
                                <AntDesignIcon style={styles.icon} name={'scan1'}/>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={() => {

                            }}>
                                <AntDesignIcon style={styles.icon} name={'message1'}/>
                            </TouchableWithoutFeedback>
                        </View>

                    </View>
                </View>
                <View style={[styles.searchContainer]} ref={ref => this.navBar = ref}>
                    <View style={styles.searchInput}>
                        <TouchableWithoutFeedback onPress={this.goSearch}>
                            <View style={styles.inputWrap}>
                                <AntDesignIcon name={'search1'} style={{fontSize: 15, marginRight: 5}}/>
                                < Text style={styles.input}>
                                    请输入标题或内容搜索...</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    <View>
                        <View style={styles.iconWrap}>
                            <TouchableWithoutFeedback onPress={this.goScannerView}>
                                <AntDesignIcon style={styles.icon} name={'scan1'}/>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback>
                                <AntDesignIcon style={styles.icon} name={'message1'}/>
                            </TouchableWithoutFeedback>
                        </View>

                    </View>
                </View>

                <ScrollView style={styles.mainWrap} showsVerticalScrollIndicator={false} onScroll={this.onScroll}>
                    <LinearGradient start={{x: 0, y: 0}}
                                    end={{x: 0, y: 1}} colors={colors} style={styles.topView}>

                    </LinearGradient>
                    <WingBlank>
                        <View style={styles.bannerWrap}>
                            {banner.length > 0 ? <Swiper
                                style={styles.swiperWrapper}
                                height={150}
                                showsButtons={false}
                                removeClippedSubviews={false} //这个很主要啊，解决白屏问题
                                autoplay={true}
                                loop={true}
                                horizontal={true}
                                paginationStyle={styles.paginationStyle}
                                dotStyle={styles.dotStyle}
                                activeDotStyle={styles.activeDotStyle}
                                autoplayTimeout={3.5}
                                autoplayDirection={true}

                                onIndexChanged={(index) => {
                                    this.bannerIndexChange(index)
                                }}
                            >
                                {
                                    banner && banner.map((d, index) => (
                                        <Image key={index}
                                               resizeMode='stretch' source={d.src} style={styles.slideImage}
                                        />
                                    ))
                                }
                            </Swiper> : null
                            }
                        </View>
                    </WingBlank>

                    <WingBlank>
                        <WhiteSpace size="sm"/>
                        <View style={styles.appContainer}>
                            <TouchableWithoutFeedback style={styles.appItem}>
                                <View style={styles.appItemContent}>
                                    <View style={[styles.iconWraps, {backgroundColor: '#FC716D'}]}>
                                        <AntDesignIcon name={'calendar'}
                                                       style={{
                                                           color: '#fff',
                                                           marginBottom: 5,
                                                           fontSize: 28,
                                                           fontWeight: 600
                                                       }}/>
                                    </View>

                                    <Text style={{color: '#000', fontSize: 14, marginTop: 5}}>
                                        日程
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback style={styles.appItem}>
                                <View style={styles.appItemContent}>
                                    <View style={[styles.iconWraps, {backgroundColor: '#38DAC8'}]}>
                                        <AntDesignIcon name={'filetext1'}
                                                       style={{
                                                           color: '#fff',
                                                           marginBottom: 5,
                                                           fontSize: 28,
                                                           fontWeight: 600
                                                       }}/>
                                    </View>

                                    <Text style={{color: '#000', fontSize: 14, marginTop: 5}}>
                                        笔记/博客
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback style={styles.appItem}>
                                <View style={styles.appItemContent}>
                                    <View style={[styles.iconWraps, {backgroundColor: '#62A9E9'}]}>
                                        <AntDesignIcon name={'zhihu'}
                                                       style={{
                                                           color: '#fff',
                                                           marginBottom: 5,
                                                           fontSize: 28,
                                                           fontWeight: 600
                                                       }}/>
                                    </View>

                                    <Text style={{color: '#000', fontSize: 14, marginTop: 5}}>
                                        知乎
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback style={styles.appItem}>
                                <View style={styles.appItemContent}>
                                    <View style={[styles.iconWraps, {backgroundColor: '#C23555'}]}>
                                        <AntDesignIcon name={'youtube'}
                                                       style={{
                                                           color: '#fff',
                                                           marginBottom: 5,
                                                           fontSize: 28,
                                                           fontWeight: 600
                                                       }}/>
                                    </View>

                                    <Text style={{color: '#000', fontSize: 14, marginTop: 5}}>
                                        bilibili
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </WingBlank>

                    <WingBlank>
                        <View style={styles.scheduleWrap}>
                            <View style={styles.scheduleTopWrap}>
                                <WingBlank style={styles.scheduleTop}>
                                    <Text
                                        style={styles.scheduleTopTitle}>今日日程
                                        ({formatDate("yyyy-MM-dd", new Date())})</Text>
                                    <TouchableOpacity>
                                        <Text style={styles.scheduleTopBtn}>
                                            全部
                                        </Text>
                                    </TouchableOpacity>
                                </WingBlank>
                            </View>
                            <View style={[styles.scheduleMainWrap, stepCount > 0 ? {} : {height: 40}]}>
                                {stepCount > 0 ?
                                    <View style={[styles.scheduleMain]}>
                                        <StepIndicator
                                            stepCount={stepCount}
                                            customStyles={customStyles}
                                            currentPosition={this.state.currentPage}
                                            labels={labels}
                                            onPress={this.onStepPress}
                                        />
                                        <ViewPager style={{flex: 1, flexGrow: 1}}
                                                   ref={viewPager => {
                                                       this.viewPager = viewPager
                                                   }}
                                                   onPageSelected={page => {
                                                       this.setState({currentPage: page.position})
                                                   }}
                                        >
                                            {schedule.map(schedule => this.renderViewPagerSchedule(schedule))}
                                        </ViewPager>
                                    </View>
                                    : <Text>
                                        今日暂无日程安排
                                    </Text>
                                }
                            </View>
                        </View>
                    </WingBlank>
                    <WingBlank>
                        <View style={styles.noteWrap}>
                            <View style={styles.noteTopWrap}>
                                <WingBlank style={styles.noteTop}>
                                    <Text
                                        style={styles.noteTopTitle}>笔记</Text>
                                    <TouchableOpacity>
                                        <Text style={styles.noteTopBtn}>
                                            全部
                                        </Text>
                                    </TouchableOpacity>
                                </WingBlank>
                            </View>
                            <View style={[styles.noteMainWrap, note.length > 0 ? {} : {height: 40}]}>
                                {note.length > 0 ?
                                    note.map(note => {
                                        return (<TouchableOpacity style={{
                                            height: 95, borderTopWidth: 1 / PixelRatio.get(),
                                            borderColor: 'rgb(211, 211, 211)',
                                        }}>
                                            <Text
                                                numberOfLines={1}
                                                style={{
                                                    fontSize: MixinStyle.fontSize.regular,
                                                    color: "#000",
                                                    marginVertical: 3,
                                                    marginTop: 6
                                                }}>{note.title}</Text>
                                            <Text
                                                numberOfLines={2}
                                                style={{
                                                    fontSize: 14,
                                                    color: "#ABABAB",
                                                    marginVertical: 3
                                                }}>
                                                {note.content}
                                            </Text>
                                            <Text style={{
                                                fontSize: 12,
                                                color: "#ABABAB",
                                                marginBottom: 15
                                            }}>
                                                {note.start_at}
                                            </Text>
                                        </TouchableOpacity>)
                                    }) :
                                    <Text>
                                        暂无笔记
                                    </Text>
                                }
                            </View>

                        </View>
                    </WingBlank>

                </ScrollView>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        backgroundColor: 'transparent'
    },

    searchContainer: {
        height: 70,
        zIndex: 3,
        width: MixinStyle.size.screenWidth,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        paddingTop: StatusBar.currentHeight,
        left: 0,
        opacity: 0,
        backgroundColor: '#38f'
    },
    searchInput: {
        flex: 1,
        height: 40,
    },
    inputWrap: {
        margin: 5,
        backgroundColor: 'white',
        flex: 1,
        height: 30,
        textAlign: 'left',
        flexDirection: 'row',
        marginLeft: 20,
        paddingLeft: 10,
        marginRight: 15,
        alignItems: 'center',
        borderRadius: 15

    },
    input: {
        color: '#ccc',
        fontSize: MixinStyle.fontSize.tiny,

    },
    iconWrap: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
    },
    icon: {
        color: '#fff',
        marginRight: 15,
        fontSize: 20
    },
    mainWrap: {
        flex: 1,
        backgroundColor: MixinStyle.color.defaultBackground,
        position: 'relative',
        paddingBottom: 40
    },
    topView: {
        height: 200,
        width: MixinStyle.size.screenWidth,
        position: 'absolute',
        left: 0,
        top: 0
    },
    bannerWrap: {
        flex: 1,
        marginTop: 80,
        height: 150,
        borderRadius: 5,
        overflow: 'hidden'
    },
    swiperWrapper: {
        flex: 1,
        height: 150,
    },
    paginationStyle: {
        bottom: 6,
    },
    dotStyle: {
        width: 22,
        height: 3,
        backgroundColor: '#fff',
        opacity: 0.4,
        borderRadius: 0,
    },
    activeDotStyle: {
        width: 22,
        height: 3,
        backgroundColor: '#fff',
        borderRadius: 0,
    },
    slideImage: {
        flex: 1,
        width: MixinStyle.size.screenWidth,
        height: 150,
    },
    appContainer: {
        flex: 1,
        height: 80,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    appItem: {
        flex: 1,
        height: 80,
        justifyContent: "center",
        alignItems: 'center'
    },
    appItemContent: {

        height: 80,
        justifyContent: "center",
        alignItems: 'center',

    },
    iconWraps: {
        borderRadius: 25,
        width: 50,
        height: 50,
        justifyContent: "center",
        alignItems: 'center'
    },
    scheduleWrap: {
        backgroundColor: '#fff',
        marginTop: 15,

        borderRadius: 3
    },
    scheduleTopWrap: {
        borderBottomWidth: 1 / PixelRatio.get(),
        borderColor: 'rgb(211, 211, 211)',
    },
    scheduleTop: {

        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 40,
    },
    scheduleTopTitle: {
        color: '#000',
        fontSize: MixinStyle.fontSize.regular,

    },
    scheduleTopBtn: {
        color: MixinStyle.buttonColor.primary,
        fontSize: MixinStyle.fontSize.regular
    },
    scheduleMainWrap: {
        height: 130,
        padding: 10,
        overflow: "hidden"
    },
    scheduleMain: {
        height: 130
    },
    noteWrap: {
        backgroundColor: '#fff',
        marginTop: 15,

        borderRadius: 3,
        marginBottom: 15
    },
    noteMainWrap: {
        padding: 10,
        overflow: "hidden",

    },
    noteTopWrap: {},
    noteTop: {

        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 40,
    },
    noteTopTitle: {
        color: '#000',
        fontSize: MixinStyle.fontSize.regular,

    },
    noteTopBtn: {
        color: MixinStyle.buttonColor.primary,
        fontSize: MixinStyle.fontSize.regular
    },
})

export default connect(mapStateToProps)(Home);
