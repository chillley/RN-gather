import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    ScrollView,
    StatusBar,
    TouchableOpacity,
    PixelRatio,
    TouchableWithoutFeedback
} from 'react-native';
import {connect} from 'react-redux';
import MixinStyle from '../../style/MixinStyle'
import AntDesignIcon from "react-native-vector-icons/AntDesign"; //使用antDesign图标
import {Input} from 'beeshell/dist/components/Input';
import {Badge, WingBlank, List, Button} from '@ant-design/react-native';

const mapStateToProps = (state) => {
    return {
        app: state.app,
    };
};

class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: ''
        };
    }


    componentDidMount() {
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('dark-content');
            StatusBar.setTranslucent(true);

        });
    }

    componentWillUnmount() {
        this._navListener.remove();

    }


    handlePressBack = () => {
        const {navigation} = this.props;
        if (navigation) {
            navigation.goBack();
        }
    }
    onSubmitEditing = () => {
        this._textInput.blur();
    }
    onPressSearch = () => {

    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar translucent={true} networkActivityIndicatorVisible={true} StatusBarAnimation={'none'}/>
                <View style={styles.header}>
                    <TouchableWithoutFeedback onPress={this.handlePressBack}>
                        <AntDesignIcon name={'left'} style={{fontSize: 25, color: '#000', marginLeft: 20}}/>
                    </TouchableWithoutFeedback>
                    <View style={{
                        height: 38,
                        flex: 1,
                        backgroundColor: 'rgba(222,222,222,0.8)',
                        borderRadius: 20,
                        marginHorizontal: 15,
                        flexDirection: "row",
                        alignItems: 'center',
                    }}>

                        <AntDesignIcon name={'search1'} style={{fontSize: 15, marginHorizontal: 5}}/>

                        <TextInput
                            ref={component => this._textInput = component}
                            style={{flex: 1}}
                            autoFocus={true}
                            returnKeyType='search'
                            placeholder="请输入想搜索的内容"
                            onChangeText={(text) => this.setState({text})}
                            value={this.state.text}
                            onSubmitEditing={this.onSubmitEditing}
                            onEndEditing={this.onPressSearch}

                        />
                    </View>

                    <Text style={{marginRight: 20}}>搜索</Text>
                </View>

                <View></View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        backgroundColor: 'transparent'
    },
    header: {
        paddingTop: StatusBar.currentHeight,
        height: 80,
        width: MixinStyle.size.screenWidth,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderBottomWidth: 1 / PixelRatio.get(),
        borderColor: 'rgb(211, 211, 211)',
    }
})

export default connect(mapStateToProps)(Search);
