import React, {Component, Fragment} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    ScrollView,
    StatusBar,
    TouchableOpacity,
    PixelRatio
} from 'react-native';
import {connect} from 'react-redux';
import MixinStyle from '../../style/MixinStyle'
import AntDesignIcon from "react-native-vector-icons/AntDesign"; //使用antDesign图标

import {Badge, WingBlank, List, Button} from '@ant-design/react-native';
import Header from "../../components/Header"


const Item = List.Item;
const Brief = Item.Brief;

const mapStateToProps = (state) => {
    return {
        app: state.app,
    };
};

class Mine extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        this._navListener = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBarStyle('light-content');
            StatusBar.setTranslucent(true);
        });
    }

    componentWillUnmount() {
        this._navListener.remove();

    }


    handleLogOut() {
        storage.remove({
            key: 'password',
        });
        storage.remove({
            key: 'user',
        });
        this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar translucent={true} networkActivityIndicatorVisible={true} StatusBarAnimation={'none'}/>
                <Header rightIcon={'message1'} title={'我的'} rightIconPress={() => {
                    alert('消息')
                }}/>

                <ScrollView style={styles.contentView} showsVerticalScrollIndicator={false}>
                    <View style={styles.topView}>
                        <WingBlank style={styles.topContent}>

                            <View style={styles.baseInfo}>
                                <TouchableOpacity>
                                    <Image style={styles.headerImage} source={{uri: this.props.app.user.avatar}}/>
                                </TouchableOpacity>
                                <View style={{marginLeft: 20}}>
                                    <Text style={{color: '#fff', fontSize: 15, marginBottom: 3}}>
                                        {this.props.app.user.name}
                                    </Text>
                                    <Text style={{color: '#fff', fontSize: 12, marginTop: 3}}>
                                        {this.props.app.user.phone}
                                    </Text>
                                </View>
                            </View>
                            <View>
                                <TouchableOpacity>
                                    <Text style={{color: '#fff', fontSize: 14}}>
                                        详细资料 >
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </WingBlank>
                    </View>
                    <View style={styles.commonMenu}>
                        <TouchableOpacity style={styles.commonMenuItem}>
                            <AntDesignIcon name={'calendar'}
                                           style={{color: '#FC716D', marginBottom: 5, fontSize: 28}}/>
                            <Text style={{color: '#000', fontSize: 14}}>
                                日程
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.commonMenuItem}>
                            <AntDesignIcon name={'filetext1'}
                                           style={{color: '#38DAC8', marginBottom: 5, fontSize: 28}}/>
                            <Text style={{color: '#000', fontSize: 14}}>
                                笔记/博客
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.commonMenuItem}>
                            <AntDesignIcon name={'contacts'}
                                           style={{color: '#9AA1F6', marginBottom: 5, fontSize: 28}}/>
                            <Text style={{color: '#000', fontSize: 14}}>
                                联系人
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.commonMenuItem}>
                            <AntDesignIcon name={'zhihu'}
                                           style={{color: '#62A9E9', marginBottom: 5, fontSize: 28}}/>
                            <Text style={{color: '#000', fontSize: 14}}>
                                知乎
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{marginTop: 15}}>
                        <List>
                            <Item
                                arrow="horizontal"

                                thumb={
                                    <AntDesignIcon
                                        name={'clockcircleo'}
                                        style={{color: '#959CF7', marginRight: 15, fontSize: 18}}/>
                                }
                                onPress={() => {
                                }}>
                                历史记录
                            </Item>
                            <Item
                                arrow="horizontal"

                                thumb={
                                    <AntDesignIcon
                                        name={'cloud'}
                                        style={{color: '#6DB6F4', marginRight: 15, fontSize: 18}}/>
                                }
                                onPress={() => {
                                }}>
                                天气
                            </Item>
                            <Item
                                arrow="horizontal"

                                thumb={
                                    <AntDesignIcon
                                        name={'shake'}
                                        style={{color: '#5AD1FF', marginRight: 15, fontSize: 18}}/>
                                }
                                onPress={() => {
                                }}>
                                日程提醒
                            </Item>
                        </List>
                    </View>
                    <View style={{marginTop: 15}}>
                        <List>
                            <Item
                                arrow="horizontal"
                                extra=""
                                thumb={
                                    <AntDesignIcon
                                        name={'github'}
                                        style={{color: '#000', marginRight: 15, fontSize: 18}}/>
                                }
                                onPress={() => {
                                }}>
                                github
                            </Item>
                            <Item
                                arrow="horizontal"
                                thumb={
                                    <AntDesignIcon
                                        name={'star'}
                                        style={{color: '#FCA802', marginRight: 15, fontSize: 18}}/>
                                }
                                onPress={() => {
                                }}>
                                收藏
                            </Item>
                            <Item
                                arrow="horizontal"
                                thumb={
                                    <AntDesignIcon
                                        name={'questioncircle'}
                                        style={{color: '#3BD5C7', marginRight: 15, fontSize: 18}}/>
                                }
                                onPress={() => {
                                }}>
                                帮助与反馈
                            </Item>
                        </List>
                    </View>
                    <View style={{marginTop: 15}}>
                        <List>
                            <Item
                                arrow="horizontal"
                                extra=""
                                thumb={
                                    <AntDesignIcon
                                        name={'setting'}
                                        style={{color: '#8A94F7', marginRight: 15, fontSize: 18}}/>
                                }
                                onPress={() => {
                                }}>
                                设置
                            </Item>
                        </List>
                    </View>

                    <WingBlank style={{marginVertical: 20}}>
                        <Button type="warning" onPress={this.handleLogOut.bind(this)}>退出</Button>
                    </WingBlank>

                </ScrollView>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative'
    },
    header: {
        width: MixinStyle.size.screenWidth,
        height: 70,
        backgroundColor: '#38f',
        alignItems: 'center',
        paddingTop: StatusBar.currentHeight,
        position: 'relative'
    },
    headerContent: {
        width: MixinStyle.size.screenWidth,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'

    },
    headerTitle: {
        color: "#fff",
        fontSize: MixinStyle.fontSize.regular,

        alignItems: 'center',
        justifyContent: 'center',
        height: 70 - StatusBar.currentHeight
    },
    headerIconsWrap: {
        position: 'absolute',
        right: 15,
        height: 70 - StatusBar.currentHeight,
        top: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
    },
    headerIcon: {
        fontSize: 20, color: '#fff'
    },
    contentView: {
        flex: 1,
        backgroundColor: MixinStyle.color.defaultBackground
    },
    topView: {
        backgroundColor: '#38f',
        height: 90,
        width: MixinStyle.size.screenWidth,
    },
    topContent: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center'
    },
    headerImage: {
        width: 60,
        height: 60,
        borderRadius: 30,
    },
    commonMenu: {
        width: MixinStyle.size.screenWidth,
        flexDirection: 'row',
        height: 80,
        borderTopWidth: 1 / PixelRatio.get(),
        borderColor: 'rgb(211, 211, 211)',
        borderBottomWidth: 1 / PixelRatio.get(),

    },
    commonMenuItem: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: "center",
    },
    baseInfo: {
        flexDirection: 'row',
        justifyContent: "space-between",
        alignItems: 'center'
    }
})
export default connect(mapStateToProps)(Mine);
