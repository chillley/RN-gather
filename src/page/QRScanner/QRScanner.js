import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    StatusBar,
} from 'react-native';
import {connect} from 'react-redux';
import MixinStyle from '../../style/MixinStyle'
import {QRScannerView} from 'react-native-qrcode-scanner-view';
import TitleBar from "../../components/TitleBar"
import ImageButton from "../../components/ImageButton"

const mapStateToProps = (state) => {
    return {
        app: state.app,
    };
};

const Images = {
    ic_wechat_scan_hl: require("../../assets/scanner/scanHl.png"),
    ic_wechat_scan_book: require("../../assets/scanner/scanBook.png"),
    ic_wechat_scan_street: require("../../assets/scanner/scanStreet.png"),
    ic_wechat_scan_word: require("../../assets/scanner/scanWord.png"),
    ic_wechat_more: require("../../assets/scanner/wechatMore.png"),
    ic_wechat_back: require("../../assets/scanner/wechatBack.png"),
}


const menuItems = [
    {icon: Images.ic_wechat_scan_hl, name: '二维码'},
    {icon: Images.ic_wechat_scan_book, name: '封面'},
    {icon: Images.ic_wechat_scan_street, name: '街景'},
    {icon: Images.ic_wechat_scan_word, name: '翻译'},
];

class QRScanner extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: '',
            focusedScreen: false,
        };
    }

    componentDidMount() {
        const {navigation} = this.props;
        this.didFocusListener = navigation.addListener('didFocus', () =>
            this.setState({focusedScreen: true}),
        );
    }

    componentWillUnmount() {
        this.didFocusListener && this.didFocusListener.remove();
    }

    renderTitleBar = () => {
        return (
            <TitleBar
                style={{marginTop: StatusBar.currentHeight}}
                titleColor={'#fff'}
                title={'二维码/条码'}

                leftIcon={Images.ic_wechat_back}
                leftIconPress={() => this.props.navigation.goBack()}
            />
        );
    };


    handlePressBack = () => {
        const {navigation} = this.props;
        if (navigation) {
            navigation.goBack();
        }
    }
    renderBottomMenuItem = (item) => (
        <View
            key={item.name}
            style={styles.viewBottomMenuItem}>
            <ImageButton style={styles.imageBottomMenu} source={item.icon}/>
            <Text style={styles.textBottomMenuItem}>{item.name}</Text>
        </View>
    );
    renderMenu = () => {
        return (
            <View style={styles.bottomMenuView}>
                {menuItems.map(item => this.renderBottomMenuItem(item))}
            </View>
        );
    };


    barcodeReceived = (event) => {
        console.log('Type: ' + event.type + '\nData: ' + event.data)
        if (event.type === "QR_CODE") {
            /*说明是二维码*/
            if (event.data.indexOf('http') !== -1) { //说明是网址进入webView页面
                this.props.navigation.replace('WebView', {uri: event.data});
            }
        }


    };


    render() {
        const {focusedScreen} = this.state;
        return (
            <View style={styles.container}>
                {
                    focusedScreen
                        ? < QRScannerView
                            rectStyle={styles.rectStyle}
                            cornerStyle={styles.cornerStyle}

                            onScanResult={this.barcodeReceived}
                            renderHeaderView={this.renderTitleBar}
                            /* renderFooterView={this.renderMenu}*/
                            scanBarAnimateReverse={false}
                        />
                        : null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        position: 'relative',
    },

    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    rectangleContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
    },

    rectangle: {
        height: 200,
        width: 200,
        borderWidth: 1,
        borderColor: '#fff',
        backgroundColor: 'transparent',
    },

    rectangleText: {
        flex: 0,
        color: '#fff',
        marginTop: 10,
    },

    imageBottomMenu: {
        height: 32,
        width: 32,
        resizeMode: 'contain',
    },

    viewBottomMenuItem: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    textBottomMenuItem: {
        color: '#fff',
        marginTop: 8,
    },
    cornerStyle: {
        borderColor: "#1A6DD5",
        height: 16,
        width: 16,
        borderWidth: 3,
    },
    bottomMenuView: {
        paddingVertical: 16,
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: "#000000",
    },
    rectStyle: {
        borderColor: "#C0C0C0",
        borderWidth: 0.5,
        height: 240,
        width: 240,
    },
})

export default connect(mapStateToProps)(QRScanner);
