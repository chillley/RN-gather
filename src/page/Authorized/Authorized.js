/**
 * @Description: 身份验证页面
 * @param:
 */


import React, {PureComponent} from 'react';
import {View, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';
import _ from 'lodash';

export class Authorized extends PureComponent {
    constructor(props) {
        super(props);
        this.hasChange();
    }

    handleLogin = async (params) => {
        try {
            const password = await storage.load({
                key: 'password',
            });
            const {dispatch} = this.props;
            dispatch({
                type: 'app/login', payload: {
                    code: params.code,
                    password: password,
                },
                callback: () => {
                    this.props.navigation.navigate('Home');
                },
            });
        } catch (e) {
            storage.remove({
                key: 'password',
            });
            storage.remove({
                key: 'user',
            });
            this.props.navigation.navigate('Login');
        }

    };

    hasChange = async () => {
        try {
            let user = await storage.load({
                key: 'user',
            });
            const oUser = JSON.parse(user);

            if (!_.isEmpty(oUser)) {
                if (_.isEmpty(this.props.app.user)) {
                    this.handleLogin(oUser);
                } else {
                    this.props.navigation.navigate('Home');
                }
            } else {
                this.props.navigation.navigate('Login');
            }
        } catch (e) {
            this.props.navigation.navigate('Login');
        }


    };

    render() {
        return (
            <View>

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.app,

    };
};
export default connect(mapStateToProps)(Authorized);
