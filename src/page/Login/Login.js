import React, {Component} from 'react';
import {StyleSheet, View, Text, PixelRatio, StatusBar, ActivityIndicator} from 'react-native';
import {connect} from 'react-redux';

import MixinStyle from '../../style/MixinStyle'
import {Button, WingBlank} from '@ant-design/react-native';
import {Form, Input, Tip} from 'beeshell';
import AntDesignIcon from "react-native-vector-icons/AntDesign"; //使用antDesign图标


class AuthLoading extends Component {
    /*propTypes 验证导航集成的导航对象是必须的*/
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            tip: null,
            password: '',
        };
    }

    componentDidMount() {

    }

    handleLogin() {
        /*登录接口登录完成后将用户信息保存到redux中*/
        const {dispatch} = this.props;
        /*dispatch 执行同步和移除的方法在于直接调用reducers还是调用effect中的异步方法*/
        this.tip.open();
        dispatch({
            type: 'app/login', payload: {
                code: this.state.username,
                password: this.state.password,
            },
            callback: (res) => {
                this.tip.close();
                if (res.success) {
                    this.props.navigation.navigate('Home');
                } else {
                    Tip.show(res.message, 1000, true);
                }

            },
        });

    }

    handleChangeValue(key, value) {
        this.setState({
            ...this.state,
            [key]: value,
        });
    }

    render() {

        const {username, password} = this.state;
        return (
            <View style={styles.container}>
                <StatusBar barStyle="dark-content" backgroundColor={'#38f'} color={'#0F4EDF'} translucent={false}/>
                <View style={styles.topWrap}></View>
                <View style={styles.formWrap}>
                    <WingBlank>
                        <Form style={styles.form}>

                            <Form.Item label={
                                <View style={styles.formLabel}>
                                    <AntDesignIcon name={'user'} style={{fontSize: 16,}}/>
                                </View>
                            } hasLine>
                                <Input
                                    value={username}
                                    placeholder='请输入手机号' style={{backgroundColor: MixinStyle.color.transparent}}
                                    onChange={(value) => {
                                        this.handleChangeValue('username', value);
                                    }}/>

                            </Form.Item>
                            <Form.Item label={
                                <View style={[styles.formLabel]}>
                                    <AntDesignIcon name={'lock'} style={{fontSize: 16,}}/>
                                </View>
                            } hasLine>
                                <Input placeholder={'请输入密码'}
                                       secureTextEntry={true}
                                       style={{
                                           backgroundColor: MixinStyle.color.transparent,
                                       }}
                                       value={password}
                                       onChange={(value) => {
                                           this.handleChangeValue('password', value);
                                       }}/>
                            </Form.Item>
                            <Button type="primary" style={styles.formButton} onPress={this.handleLogin.bind(this)}>
                                登录
                            </Button>

                        </Form>
                    </WingBlank>
                </View>
                <View style={styles.bottomWrap}>
                    <View>
                        <Text style={[styles.thirdParty]}>————第三方登录————</Text>
                    </View>
                    <View style={styles.iconsWrap}>
                        <AntDesignIcon name={'QQ'} style={styles.icon}/>
                        <AntDesignIcon name={'wechat'} style={styles.icon}/>
                        <AntDesignIcon name={'alipay-circle'} style={styles.icon}/>
                    </View>
                </View>
                <Tip
                    ref={(c) => {
                        this.tip = c;
                    }}
                    body={
                        <View>
                            <ActivityIndicator size='small' color='#fff'/>
                            <Text style={{color: '#fff', textAlign: 'center', marginTop: 10}}>加载中...</Text>
                        </View>
                    }
                    cancelable={true}>
                </Tip>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        flex: 1,
    },
    topWrap: {
        height: 130,
        width: MixinStyle.size.screenWidth,
        backgroundColor: '#38f'
    },
    formWrap: {
        marginTop: 10,
    },
    formLabel: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 20,

    },
    form: {
        backgroundColor: MixinStyle.color.transparent,
        borderRadius: 5,
        paddingVertical: 20
    },
    formButton: {
        marginTop: 40
    },
    bottomWrap: {
        height: 100,
        width: MixinStyle.size.screenWidth,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
        marginTop: 40

    },
    thirdParty: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconsWrap: {

        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    icon: {
        color: '#B7B8BB',
        fontSize: 30,
        marginHorizontal: 20
    }
});
const mapStateToProps = (state) => {
    return {
        app: state.app,

    };
};
export default connect(mapStateToProps)(AuthLoading);
