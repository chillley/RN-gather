/**
 * @Description: 路由跳转页面总控制页面
 * @param: 从新架构react-native的路由页面封装通用跳转方法和跳转页面方法给与通用参数传递方式
 */

import {
    createStackNavigator,
    createSwitchNavigator,
    createAppContainer,
    createBottomTabNavigator,

} from 'react-navigation';

import BottomBar from './BottomBar';
import Login from '../page/Login/Login';
import HomePage from '../page/Home/Home';
import Authorized from '../page/Authorized/Authorized';
import Mine from '../page/Mine/Mine';
import Search from '../page/Home/Search'
import QRScanner from '../page/QRScanner/QRScanner'
import WebView from '../page/WebView/WebView'
import StackViewStyleInterpolator from 'react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator';

const Home = createBottomTabNavigator({
    HomePage: HomePage,
    HomePage1: HomePage,
    HomePage2: HomePage,

    Mine: Mine,
}, {
    initialRouteName: 'HomePage',
    tabBarComponent: BottomBar,
    mode: 'modal',
    navigationOptions: ({navigation}) => {
        return {
            header: null,
        };
    },
});

const App = createStackNavigator({
        Home: Home,
        Search: {
            screen: Search,
            navigationOptions: ({navigation}) => {
                return {
                    header: null,
                };
            },
        },
        QRScanner: {
            screen: QRScanner,
            navigationOptions: ({navigation}) => {
                return {
                    header: null,
                };
            },
        },
        WebView: {
            screen: WebView,
            navigationOptions: ({navigation}) => {
                return {
                    header: null,
                };
            },
        },

    },
    {
        initialRouteName: 'Home',
        defaultNavigationOptions: {
            gesturesEnabled: false,
        },
        header: null,
        transitionConfig: (transitionProps, prevTransitionProps) => {
            const IOS_MODAL_ROUTES = [''];
            const isModal = IOS_MODAL_ROUTES.some(
                screenName =>
                    screenName === transitionProps.scene.route.routeName ||
                    (prevTransitionProps &&
                        screenName === prevTransitionProps.scene.route.routeName),
            );
            return {
                screenInterpolator: isModal ? StackViewStyleInterpolator.forVertical : StackViewStyleInterpolator.forHorizontal,
            };
        },
    })
//创建身份验证登录流程

const RootNavigator = createSwitchNavigator({
        App: App,
        Authorized: Authorized,
        Login: Login,
    },
    {
        initialRouteName: 'Authorized',
    },
);
export default createAppContainer(RootNavigator);
