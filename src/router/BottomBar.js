import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    Text,
    Image,
    PixelRatio,
} from 'react-native';

import AntDesignIcon from "react-native-vector-icons/AntDesign"; //使用antDesign图标


const tabItems = [
    {
        title: '首页',
        icon: 'home',
    },
    {
        title: '工作台',
        icon: 'appstore-o',
    }
    , {
        title: '发现',
        icon: 'indent-left',
    }
    ,
    {
        title: '我的',
        icon: 'user',
    },

];


export default class TabBar extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        const {
            navigation,
        } = this.props;

        const {
            routes,
        } = navigation.state;

        /*js控制导航跳转*/
        const jumpToIndex = (index) => {
            navigation.navigate(routes[index].routeName);
        };
        return (
            <View style={styles.container}>
                {
                    routes && routes.map((route, index) => {
                        const focused = index === navigation.state.index;
                        return (<TabBarItem
                                key={index}
                                route={route}
                                index={index}
                                focused={focused}
                                jumpToIndex={jumpToIndex}
                            />
                        );
                    })
                }
            </View>
        );
    }
}


class TabBarItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {index, focused, jumpToIndex} = this.props;
        let item = tabItems[index];
        let color = focused ? '#33a3f4' : '#949494';

        return (
            <TouchableWithoutFeedback style={styles.itemWrap} key={index} onPress={
                () => {
                    jumpToIndex(index);
                }
            }>
                <View
                    style={[styles.item]}>
                    <AntDesignIcon name={item.icon} style={{fontSize: 20, color: color}}></AntDesignIcon>
                    <Text style={{color: color, fontSize: 12}}>{item.title}</Text>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: 53,
        backgroundColor: '#fff',
        borderTopWidth: 1 / PixelRatio.get(),
        borderColor: 'rgb(211, 211, 211)',
    },
    itemWrap: {
        flex: 1,
        alignItems: 'center',
        height: 53,

    },
    item: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',

    }

});
