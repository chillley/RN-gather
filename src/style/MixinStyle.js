import {Dimensions, Platform, DeviceInfo, NativeModules} from 'react-native';

const {width, height} = Dimensions.get('window');
const {PlatformConstants = {}} = NativeModules;
const {minor = 0} = PlatformConstants.reactNativeVersion || {};
const IS_IPHONE_SE = width < 350;
const IS_IPAD_PRO_97 = height === 480;
const X_WIDTH = 375;
const X_HEIGHT = 812;
const IS_IPHONE_X = (() => {
    return false;
    if (Platform.OS === 'web') {
        return false;
    }
    if (minor >= 50) {
        return DeviceInfo.isIPhoneX_deprecated;
    }
    return (
        Platform.OS === 'ios' &&
        ((height === X_HEIGHT && width === X_WIDTH) ||
            (height === X_WIDTH && width === X_HEIGHT))
    );
})();
const IS_IPHONE_PLUS = height === 736;

let unit;
if (IS_IPHONE_PLUS) {
    unit = 7;
} else if (IS_IPHONE_SE) {
    unit = 5;
} else {
    unit = 6;
}

const color = {
    transparent: 'transparent',
    defaultBackground: '#F2F2F2', // '#FCFDFF',
    fontDefaultColor: '#2a3a63',
};
const buttonColor = {
    primary: '#1890ff'
}

const size = {
    screenWidth: width,
    screenHeight: height,
};

const fontSize = {
    tiny: IS_IPHONE_SE ? 8 : 12,
    regular: IS_IPHONE_SE ? 14 : 16,
    big: IS_IPHONE_SE ? 16 : 18,
    large: IS_IPHONE_SE ? 26 : 30,
    huge: IS_IPHONE_SE ? 32 : 36,
};

const font = {
    title: {
        fontFamily: 'Avenir Next',
        color: color.black,
        fontSize: fontSize.large,
    },
    regular: {
        fontFamily: 'Avenir Next',
        color: color.white1,
        fontSize: fontSize.regular,
    },
    bold: {
        fontFamily: 'Avenir Next',
        fontWeight: '500',
        color: color.black,
        fontSize: fontSize.regular,
    },
};
export default {
    color,
    size,
    font,
    fontSize,
    IS_IPHONE_SE,
    IS_IPHONE_X,
    IS_IPHONE_PLUS,
    IS_IPAD_PRO_97,
    unit,
    buttonColor
};
