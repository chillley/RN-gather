import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View, TouchableWithoutFeedback} from 'react-native';
import PropTypes from 'prop-types';
import AntDesignIcon from "react-native-vector-icons/AntDesign";
import MixinStyle from "../style/MixinStyle";

/*已決定所有StatusBar都会已实现沉浸式header会留出一部分*/

class Header extends Component {

    //组件使用类型检测

    static propTypes = {
        leftIcon: PropTypes.string,
        rightIcon: PropTypes.string,
        leftIconPress: PropTypes.func,
        rightIconPress: PropTypes.func,
        style: PropTypes.object,

        title: PropTypes.string,
    };


    render() {
        const titleColor = this.props.titleColor || {color: "#fff", fontSize: MixinStyle.fontSize.regular};
        return (
            <View style={styles.header}>
                <View style={styles.headerLeftIconWrap}>
                    <TouchableWithoutFeedback style={{flex: 1}} onPress={this.props.leftIconPress}>
                        <AntDesignIcon name={this.props.leftIcon}
                                       style={styles.headerIcon}/>
                    </TouchableWithoutFeedback>
                </View>

                <View style={styles.headerContent}>
                    <View style={styles.headerTitle}>
                        <Text numberOfLines={1} style={titleColor} >{this.props.title}</Text>
                    </View>


                </View>
                <View style={styles.headerIconsWrap}>
                    <TouchableWithoutFeedback style={{flex: 1}} onPress={this.props.rightIconPress}>
                        <AntDesignIcon name={this.props.rightIcon}
                                       style={styles.headerIcon}/>
                    </TouchableWithoutFeedback>
                </View>

            </View>)
    }
}

const styles = StyleSheet.create({
    header: {
        width: MixinStyle.size.screenWidth,
        height: 70,
        backgroundColor: '#38f',
        alignItems: 'center',
        paddingTop: StatusBar.currentHeight,
        position: 'relative',
        flexDirection: 'row'
    },
    headerContent: {
        width: MixinStyle.size.screenWidth,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        flex: 1,
        paddingHorizontal: 45
    },
    headerTitle: {
        color: "#fff",
        fontSize: MixinStyle.fontSize.regular,
        alignItems: 'center',
        justifyContent: 'center',
        height: 70 - StatusBar.currentHeight
    },

    headerLeftIconWrap: {
        position: 'absolute',
        left: 10,
        width: 30,
        height: 70 - StatusBar.currentHeight,
        top: StatusBar.currentHeight,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
    },
    headerIconsWrap: {
        position: 'absolute',
        right: 10,
        width: 30,
        height: 70 - StatusBar.currentHeight,
        flexDirection: 'row',
        top: StatusBar.currentHeight,
        alignItems: 'center',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
    },
    headerIcon: {
        color: '#fff',
        fontSize: 18
    },
})

export default Header
