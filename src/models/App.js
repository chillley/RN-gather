import {login} from '../server/api';


export default {
    namespace: 'app',
    state: {
        user: {},
    },
    reducers: {
        /*处理action*/
        setUserInfo(state, {payload: data}) {

            return {...state, user: {...data}};
        },
    },
    effects: {
        /*处理异步action 主要使用redux-saga*/
        * login(payload, {put, call}) {
            try {
                const res = yield call(() => login({...payload.payload}));


                if (res.success) {
                    /*保存数据到本地存储中*/
                    global.storage.save({
                        key: 'user',
                        data: JSON.stringify(res.data),
                        expires: null,
                    });
                    global.storage.save({
                        key: 'password',
                        data: payload.payload.password,
                        expires: null,
                    });

                    yield  put({
                        type: 'setUserInfo',
                        payload: res.data,
                    });

                } else {
                    yield  put({
                        type: 'setUserInfo',
                        payload: res,
                    });
                }
                payload.callback(res);
            } catch (e) {
                console.log(e);
            }
        },
    },
};
