/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
//采用dva架构完成数据处理


import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {create} from 'dva-core';
import models from './src/models';
import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';
import RootNavigator from './src/router';
import {Provider} from 'react-redux';


console.ignoredYellowBox = ['Warning: BackAndroid is deprecated. Please use BackHandler instead.', 'source.uri should not be an empty string', 'Invalid props.style key'];
console.disableYellowBox = true; // 关闭全部黄色警告


let storage = new Storage({
    size: 1000,
    storageBackend: AsyncStorage,
    defaultExpires: null,
    enableCache: true,
});
// 全局变量
global.storage = storage;

const dvaApp = create({
    models: models,
    onError(e) {
        console.log('onError', e);
    },
});
if (!global.registered) {
    models.forEach(model => dvaApp.model(model));
}

global.registered = true;
dvaApp.start();

const store = dvaApp._store;

dvaApp.start = (container) => {
    return () => {
        return (
            <Provider store={store}>
                {container}
            </Provider>
        );
    };
};


const App = dvaApp.start(<RootNavigator/>);

export default App;

/*
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});*/
